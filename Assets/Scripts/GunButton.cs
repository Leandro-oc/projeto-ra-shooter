﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class GunButton : MonoBehaviour, IVirtualButtonEventHandler {

    public float BulletVelocity = 20f;

    public GameObject bulletPrefab;
    public Transform spawnObject;

    AudioSource GunShot;
    public AudioClip shot, reload;

    RaycastHit hit;
    LayerMask layerMask;

    void Start()
    {
        VirtualButtonBehaviour[] vbs = GetComponentsInChildren<VirtualButtonBehaviour>();

        GunShot = GetComponentInChildren<AudioSource>();

        for (int i = 0; i < vbs.Length; i++)
        {
            vbs[i].RegisterEventHandler(this);
        }
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        //Debug.Log(vb.VirtualButtonName + " Pressed");
        GunShot.PlayOneShot(shot);

        if (Physics.Raycast(spawnObject.position, spawnObject.forward, out hit, Mathf.Infinity, LayerMask.GetMask("Target")))
        {
            //Executa a ação da destruição da bolinha
            //hit.collider.gameObject.SetActive(false);
            hit.transform.GetComponent<Bomb>().Blow();
        };
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        GunShot.PlayOneShot(reload);
    }
}
