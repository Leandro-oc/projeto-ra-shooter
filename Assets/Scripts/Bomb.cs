﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;

public class Bomb : MonoBehaviour {

    AudioSource audioSource;

    public float Force;

    public bool Blown;
    public Transform[] Positions;
    public static List<Bomb> Bombs;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        Blown = false;
    }

    public void Blow()
    {
        if (!Blown)
        {
            audioSource.Play();

            if (transform.localScale.sqrMagnitude > 0.283f)
            {
                for (int i = 0; i < 4; i++)
                {
                    GameObject bomb = Instantiate(gameObject, Positions[i].position, Quaternion.identity);
                    bomb.transform.SetParent(transform.parent, true);
                    bomb.transform.localScale = transform.localScale * 0.75f;
                    bomb.transform.localRotation = transform.localRotation;
                    bomb.GetComponent<Rigidbody>().AddForce((Positions[i].position - transform.position).normalized * Force, ForceMode.Impulse);
                    Bombs.Add(bomb.GetComponent<Bomb>());
                }
            }

            Blown = true;
            GameManager.Instance.AddToScore(1);
            Color[] rand = new Color[] { Color.green, Color.magenta, Color.yellow };
            GetComponent<MeshRenderer>().sharedMaterial.color = rand[Random.Range(0, rand.Length)];

            Bombs.Remove(this);
            LeanTween.scale(gameObject, Vector3.zero, 0.3f).setEaseInBack().setDestroyOnComplete(true);

            if (Bombs.Count == 0) //ganhou
                GameManager.Instance.GameOver(true);
        }
    }

    public void Die()
    {
        LeanTween.scale(gameObject, Vector3.zero, 0.25f).setEaseInBack();
    }
}
