﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager Instance;

    public Bomb firstBomb;
    public RectTransform GameOverScreen;
    public RectTransform BlackBg;

    public Text ScoreText;
    public Text finalScoreText;
    public Text bestScoreText;
    public Text endGameText;

    private int score;
    private int bestScore;

    public void Awake()
    {
        Instance = this;
        Bomb.Bombs = new List<Bomb>();
        Bomb.Bombs.Add(firstBomb);
        score = 0;
        bestScore = PlayerPrefs.GetInt("ra-best", 0);
    }

    public void GameOver(bool win)
    {
        if (!win)
        {
            foreach (Bomb bomb in Bomb.Bombs)
                bomb.Die();
        }

        LeanTween.color(BlackBg, Color.black * 0.75f, 0.25f);
        GameOverScreen.gameObject.SetActive(true);

        endGameText.color = win ? Color.green : Color.red;
        endGameText.text = win ? "VENCEU!" : "PERDEU!";

        finalScoreText.text = score.ToString("D3");
        bestScoreText.text = bestScore.ToString("D3");

        PlayerPrefs.SetInt("ra-best", bestScore);
        PlayerPrefs.Save();
    }

	public void Restart()
    {
        SceneManager.LoadScene(0);
    }

    public void AddToScore(int value)
    {
        score += value;
        ScoreText.text = score.ToString("D3");

        if (score > bestScore)
            bestScore = score;
    }
}
