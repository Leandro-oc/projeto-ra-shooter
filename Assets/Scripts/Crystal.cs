﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crystal : MonoBehaviour {

    public static bool CalledGameOver;

    public void Awake()
    {
        CalledGameOver = false;
    }
    void OnCollisionEnter(Collision coll)
    {
        if (coll.transform.CompareTag("Bomb"))
        {
            if (!CalledGameOver)
            {
                Debug.Log("BOOOOOM!!");
                GameManager.Instance.GameOver(false);
                CalledGameOver = true;
            }
        }
    }
}
