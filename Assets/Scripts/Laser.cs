﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Laser : MonoBehaviour {

    LineRenderer mLineRenderer;
    RaycastHit mHit;


	void Start () {

        mLineRenderer = GetComponent<LineRenderer>();

    }
	
	void Update () {

        if(Physics.Raycast(transform.position, transform.forward, out mHit))
        {
            if (mHit.collider)
            {
                //Debug.Log("My Z: " + transform.position.z + "Other Z: " + mHit.transform.position.z);
                mLineRenderer.SetPosition(1, new Vector3(0, 0, 1 + mHit.distance/100));
            }
        }
        else
        {
            mLineRenderer.SetPosition(1, new Vector3(0, 0, 5000));
        }

	}
}
